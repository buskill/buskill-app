# BusKill App

This is the codebase for our cross-platform (CLI and) GUI app for [BusKill](https://www.buskill.in).

BusKill is a laptop kill cord that can trigger your computer to lock or shutdown when it's physically seperated from you.

<p align="center">
  <img src="images/busKill_demo.gif?raw=true" alt="BusKill Demo Video"/>
</p>

For more information on how to buy or build your own BusKill cable, see the [BusKill Website](https://www.buskill.in):

 * [https://www.buskill.in](https://www.buskill.in)

![](images/busKill_featuredImage.jpg)

# See GitHub

At the time of writing, GitLab doesn't offer shared runners on MacOS. Because our CI tools need to build BusKill in a MacOS environment (in addition to Linux and Windows), our code for `buskill-app` is *actually* on GitHub, not GitLab.

 * [https://github.com/BusKill/buskill-app](https://github.com/BusKill/buskill-app)

*This* repo currently only functions as a redundant mirror for hosting our update metadata files for the BusKill app's built-in update feature.

If GitLab ever offers free shared runners on the MacOS platform, then we may migrate from [our GitHub repo](https://github.com/BusKill/buskill-app) to here in the future.

# License

The contents of this repo are dual-licensed. All code is GPLv3 and all other content is CC-BY-SA.

# For more Information

See https://tech.michaelaltfield.net/2020/01/02/buskill-laptop-kill-cord-dead-man-switch/
